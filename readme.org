#+Title: Programmation asynchrone et callback
#+Author: Pierre Gambarotto
#+Email: pierre.gambarotto@enseeiht.fr
#+OPTIONS: num:nil reveal_title_slide:auto toc:nil
#+OPTIONS: reveal_center:nil
#+REVEAL_THEME: sky
#+REVEAL_PLUGINS: (markdown notes zoom)

#+REVEAL_EXTRA_CSS: ./local.css
#+REVEAL_MARGIN: 0.01
#+REVEAL_ROOT: http://cdn.jsdelivr.net/reveal.js/3.0.0/

# missing export option ? eval (require 'ox-reveal)
# C-c C-e R B|R to export as a reveal js presentation
#+LATEX_CLASS: article
#+LaTeX_CLASS_OPTIONS: [a4paper]
#+LaTeX_CLASS_OPTIONS: [12pt]
#+LaTeX_CLASS_OPTIONS: [listings, listings-sv]
#+LANGUAGE: fr
#+LATEX_HEADER: \usepackage[francais]{babel}

# C-c C-e l l/p/o to export as latex/pdf document

# tangle a block : C-u C-c C-v C-t (C-ucvt)

# include an image

# toggle dispaly inline image: C-c C-x C-v

* Flux d'un programme
- impératif :: une instruction puis l'autre
- fonctionnel :: un appel de fonction après l'autre

** Javascript runtime
Une machine virtuelle js (navigateur ou node.js) dédie un /thread/ à l'exécution
de code javascript.

- thread :: assure l'exécution d'un programme/tâches
- multithread :: on peut faire du /parralélisme/

Le runtime javascript est monothread: un seul programme javascript est exécuté à
la fois.

** Pile d'appel des fonctions

/callstack/ en anglais: permet de se souvenir d'où on en est dans les appels de
fonction

- appel d'une fonction: on empile : nom de la fonction, valeur donnée à chaque
  argument
- fin d'une fonction: on dépile
- pile LIFO: Last In, First Out

** Exemple

#+BEGIN_SRC javascript
function one() {
  console.lo('stuff')
}

function two(){
  one()
}

function go(){
  two()
}

go()
#+END_SRC

#+REVEAL: split

Pile d'appel: on commence toujours par la fonction ~main~, représentant le
programme principal:

#+ATTR_REVEAL: :frag (appear) :frag_idx (5 4 3 2 1)
- console.log
- one
- two
- go
- main
#+REVEAL: split

Pile d'appel: on commence toujours par la fonction ~main~, représentant le
programme principal:

#+ATTR_REVEAL: :frag (fade-out) :frag_idx (1 2 3 4 5)
- console.log
- one
- two
- go
- main


** Interlude: gestion des erreurs en js
Identique à python, java …

- [[https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Contr%25C3%25B4le_du_flux_Gestion_des_erreurs#L'instruction_throw][throw]] pour soulever une exception
- [[https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Contr%25C3%25B4le_du_flux_Gestion_des_erreurs#L'instruction_try...catch][try…catch]] pour récupérer une exception
- [[https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error#Error_types][les types pour les Erreurs]]

** Exemple: first(a,f)

- renvoie le premier élément de =a= pour lequel =f= est /truthy/
- soulève l'exception =Error("not found")= dans les autres cas

#+BEGIN_SRC javascript
  function first(a, f) {
    if (a.length === 0)
      throw new Error("not found")
    if (f(a[0]))
      return a[0]
    return first(a.slice(1),f)
  }

  try {
    first([])
  } catch(e) {
    console.log(e.message)
  }
#+END_SRC

** Pile d'appel et erreur

Quand un erreur /non rattrapée/ se produit, le /runtime/ javascript affiche la
pile d'appel:

#+BEGIN_SRC javascript :tangle /tmp/callstack_error.js
function err(){
  throw new Error('oooohhhh nooooo !')
}

function one() {
  err()
}

function two(){
  one()
}

function go(){
  two()
}

go()
#+END_SRC

#+REVEAL: split
#+BEGIN_EXAMPLE
Error: oooohhhh nooooo ! debugger eval code:2:9
    err debugger eval code:2
    one debugger eval code:6
    two debugger eval code:10
    go debugger eval code:14
    <anonyme> debugger eval code:17
#+END_EXAMPLE



* [A]Synchrone

- synchrone: on sait quand ça finit
- asynchrone: on ne sait pas

Dans un appel de sous-programme:

- bloquant : on attend le résultat
- non bloquant : on n'attend pas le résultat

Abus de langage:

- programmation synchrone: résoudre un appel après l'autre, attente 
- programmation asynchrone: lancer un appel, ne pas attendre, continuer le
  programme courant

** I/O

- Entrées/Sorties : interaction de votre programme avec le monde
- Entrée/Sorties d'un navigateur: réseau, interaction utilisateur, horloge
- Entrées/Sorties synchrones : bloquant
- Entrées/Sorties asynchrones : non bloquant

** Navigateur, exemple synchrone
#+begin_src javascript
let choice = confirm('Yo user, do you confirm ?')
if (choice)
  console.log('user is ok')
else
  console.log('user vehemently disagrees')
console.log('I have been blocked')
#+end_src

** Version asynchrone

Gestion par événement

- demander une opération asynchrone
- spécifier le traitement à réaliser à la fin de l'opération

On ne décide pas du début de l'opération

On ne décide pas de la fin de l'opération

** Navigateur, exemple asynchrone
#+begin_src javascript
let ok = document.createElement('button')
ok.innerHTML = 'ok'
ok.addEventListener('click', () => console.log('user is ok'))
let ko = document.createElement('button')
ko.innerHTML = 'Annuler'
ko.addEventListener('click', () => console.log('user  vehemently disagrees'))
document.body.appendChild(ok)
document.body.appendChild(ko)
console.log('not blocked !!!!!')
#+end_src

** callback

- Fontion donnée par le développeur
- au moment de la demande d'une opération asynchrone /fournie/ par la VM
- exécutée par la VM plus tard, quand le résultat est disponible

#+begin_src javascript
// call an async function, give callback as reference
function callback(…){…}
asyncop(some_args, callback)

// call an async function, give callback as value
asyncop(some_args, function(…){
  …
})

// call an async function, give callback as value, ES6 style
asyncop(some_args, (…) => {…})
#+end_src

** API navigateur
Fonctionnalités apportés par le navigateur
- interactions utilisateur (clavier, souris …)
- réseau (ajax, webrtc, websocket …)
- autres API (géo loc, caméra, micro, vibrations, service worker …)

Chaque fonctionnalité est utilisable par une API JS:
- l'appel à l'API est fait dans le code exécuté par le thread principal
- le thread principal assure l'affichage !!!!!!!!
- chaque appel peut être traité par un thread spécifique, géré par le navigateur
- chaque appel doit spécifier un /callback/
- le /callback/ sera exécuté à la fin de l'appel, par le thread principal

* Se méfier de son intuition

#+BEGIN_SRC javascript
function a(){
  console.log('a')
  b()
}

function b(){
  setTimeout(function(){console.log('b')},0)
  c()
}

function c(){
  console.log('c')
}

a()
#+END_SRC

- =setTimeout(cb, milliseconds)= : exécute la fonction =cb= après écoulement du délai
- quel résultat d'après vous ?
- vérifiez votre intuition !


* Javascript Event Loop

- Javascript: un seul fil d'exécution pour le code
- comment ne pas bloquer ce fil d'exécution lors d'opérations /longues/, comme
  les IO ?

=> on délègue les opérations bloquantes à la VM

** Résolution 

- code js : demande à la VM des opérations asynchrones
- code js : définit les callbacks
- VM : gère les opérations asynchrones
- VM : empile dans une /task queue/ les callbacks à exécuter
- VM : quand la pile d'appel du thread principal est vide: 
  - choisit un callback
  - le fait exécuter par le thread principal

Nombre de threads exécutant le code js : 1 !!!!!!

Nombre de threads pour gérer les opérations asynchrones : plusieurs

Le thread exécutant le js n'est jamais interrompu

** Thread js

1. code initial du programme :
  - demande d'opérations asynchrones 
  - définit des callbacks
2. VM : éxécute les opérations asynchrones
  - thread I/O
  - empile des callback
3. VM décide du callback à exécuter, en fonction des événements remontées par
   les threads I/O
4. thread JS: exécute le code du callback
5. goto 3 

Si plus aucun événement empilé : programme fini 

* Environnement d'exécution javascript

Exécuter du code javascript => une machine virtuelle

Environnement d'exécution (node ou navigateur):

- un seul thread d'exécution pour votre code !
  - callstack
- task queue: empile les callback à exécuter
- une boucle d'évaluation globale qui dépile la queue et exécute les actions différées

* Programmation asynchrone

Différer l'exécution de code jusqu'à l'occurence d'un évènement

** Actions différées

Définies par:
- l'évènement qui enclenchera le début de la résolution
- le code à appeler : en javascript, ce sera une fonction, appelée callback ou handler.

** Événements

- gestion sous le contrôle de l'environnement

- dépendent de l'environnement d'exécution :

** navigateur : 
  - input utilisateur (souris, clavier), 
  - flux réseau (ajax)
  - Service Worker
  - globalement toute API offerte par le navigateur

** node.js : 
  - évènements liés à des flux I/O (fichiers, socket réseau) 
  - début de flux, données disponibles, fin de flux

** Commun
Certains évèrements se retrouvent sur les 2 environnements :
- timer qui arrive à échéance setTimeout, setInterval

** Exemple node.js

#+BEGIN_SRC javascript
let options = {
  hostname: 'gamba.perso.enseeiht.fr',
  port: 80,
  path: '/applications_internet/index.html',
  method: 'GET',
};
let req = http.request(options, (res) => {
  console.log(`STATUS: ${res.statusCode}`);
  console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
  res.setEncoding('utf8');
  res.on('data', (chunk) => {
    console.log(`BODY: ${chunk}`);
  });
  res.on('end', () => {
    console.log('No more data in response.');
  });
});

req.on('error', (e) => {
  console.log(`problem with request: ${e.message}`);
});
#+END_SRC

* EventLoop

Boucle d'exécution principale

#+BEGIN_SRC javascript
runYourScript();
while (atLeastOneCallbackIsQueued) {
  fireNextQueuedCallback();
};
#+END_SRC

* Écrire du code asynchrone

Une seule possibilité :

- utiliser des fonctions asynchrones fournies

Pour cela :

- pas de syntaxe pour reconnaître une fonction asynchrone
- une fonction asynchrone a un callback en argument
- une fonction avec une fonction en argument n'est pas forcément asynchrone 
  - par exemple, ~map~ sur les tableaux
- documentation pour les arguments du callback 

#+REVEAL: split 

- enrober une fonction dans ~setTimeout~ pour la rendre asynchrone

#+BEGIN_SRC javascript
  function async(message){
    setTimeout(() => console.log(message), 0)
  }
#+END_SRC

=> déferre l'exécution jusqu'à la fin du code courant


* Menteur !

- vision simplifiée pour comprendre …
- [[http://latentflip.com/loupe/?code=JC5vbignYnV0dG9uJywgJ2NsaWNrJywgZnVuY3Rpb24gb25DbGljaygpIHsKICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gdGltZXIoKSB7CiAgICAgICAgY29uc29sZS5sb2coJ1lvdSBjbGlja2VkIHRoZSBidXR0b24hJyk7ICAgIAogICAgfSwgMjAwMCk7Cn0pOwoKY29uc29sZS5sb2coIkhpISIpOwoKc2V0VGltZW91dChmdW5jdGlvbiB0aW1lb3V0KCkgewogICAgY29uc29sZS5sb2coIkNsaWNrIHRoZSBidXR0b24hIik7Cn0sIDUwMDApOwoKY29uc29sZS5sb2coIldlbGNvbWUgdG8gbG91cGUuIik7!!!PGJ1dHRvbj5DbGljayBtZSE8L2J1dHRvbj4%253D][vision simplifiée]]
- [[https://medium.com/the-node-js-collection/what-you-should-know-to-really-understand-the-node-js-event-loop-and-its-metrics-c4907b19da4c][la réalité pour node.js]]


